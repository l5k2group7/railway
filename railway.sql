-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 03, 2017 at 06:13 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `railway`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `idcard` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `idcard`, `phone`) VALUES
(1, 'lee', '123456', '123456'),
(2, 'tom', '234567', '234567');

-- --------------------------------------------------------

--
-- Table structure for table `route`
--

CREATE TABLE `route` (
  `id` int(11) NOT NULL,
  `routenumber` text NOT NULL,
  `start` text NOT NULL,
  `end` text NOT NULL,
  `time` time NOT NULL,
  `class` text NOT NULL,
  `take` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `route`
--

INSERT INTO `route` (`id`, `routenumber`, `start`, `end`, `time`, `class`, `take`) VALUES
(1, 'T125', 'chengdu', 'shanghai', '02:10:13', 'slow', '26th'),
(2, 'T126', 'chengdu', 'shanghai', '05:20:00', 'slow', '26th'),
(3, 'D1630', 'chengdu', 'shanghai', '08:30:00', 'fast', '15th'),
(4, 'D1314', 'chengdu', 'shanghai', '12:05:00', 'fast', '15th'),
(5, 'T137', 'chengdu', 'shanghai', '18:40:00', 'slow', '26th'),
(6, 'T337', 'chengdu', 'chongqing', '06:20:00', 'slow', '4th'),
(7, 'D1215', 'chengdu', 'chongqing', '08:10:00', 'fast', '2th'),
(8, 'T339', 'chengdu', 'chongqing', '12:50:00', 'slow', '4th'),
(9, 'D5453', 'chengdu', 'chongqing', '16:10:00', 'fast', '2th'),
(10, 'D1236', 'chengdu', 'chongqing', '20:25:00', 'fast', '2th');

-- --------------------------------------------------------

--
-- Table structure for table `seats`
--

CREATE TABLE `seats` (
  `id` int(11) NOT NULL,
  `level` text NOT NULL,
  `seat_number` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ticket`
--

CREATE TABLE `ticket` (
  `id` int(11) NOT NULL,
  `customersid` int(11) NOT NULL,
  `trainid` int(11) NOT NULL,
  `seatsid` int(11) NOT NULL,
  `date` date NOT NULL,
  `money` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `train`
--

CREATE TABLE `train` (
  `id` int(11) NOT NULL,
  `routeid` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `route`
--
ALTER TABLE `route`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `train`
--
ALTER TABLE `train`
  ADD PRIMARY KEY (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
